from abc import ABC
from typing import TypeVar


class Parent(ABC):
    ...


class Child(Parent):
    ...


class GrandChild(Child):
    ...


class Exogenous:
    ...


# The generic type must be of subtype `Parent`
T = TypeVar('T', bound=Parent)


def process_object(item: T) -> T:
    return item


process_object(Parent())
process_object(Child())
process_object(GrandChild())

process_object(Exogenous())
