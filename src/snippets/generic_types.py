from typing import (
    List,
    TypeVar,
)

# Declare the generic type
T = TypeVar('T')


def expand_list(items: List[T]) -> List[T]:
    return items + items[::-1]


items_int = expand_list([1, 2, 3])
items_float = expand_list([1.1, 2.2, 3.3])


def use_list_of_int(items: List[int]) -> None:
    ...


use_list_of_int(items_int)
use_list_of_int(items_float)
