from typing import NewType

MessageID = NewType('MessageID', str)
CommentID = NewType('CommentID', str)


# `message_id` and `comment_id` share the same type.
# How to make sure the message ID will be passed first, and the commend ID second?
def process_v1(
    message_id: str,
    comment_id: str,
) -> None:
    print(f'{message_id=}/{comment_id=}')


def process_v2(
    message_id: MessageID,
    comment_id: CommentID,
) -> None:
    print(f'{message_id=}/{comment_id=}')


message_id: MessageID = MessageID('msg_01')
comment_id: CommentID = CommentID('comment_02')

process_v1(
    message_id,
    comment_id,
)
# The parameters are switched: no error is reported.
process_v1(
    comment_id,
    message_id,
)

process_v2(
    message_id,
    comment_id,
)
# The parameters are switched (note this does not prevent execution).
process_v2(
    comment_id,
    message_id,
)
