from typing import (
    Protocol,
    TypeVar,
)

T = TypeVar(
    'T',
    # The generic type can be either `int` or `str`
    int,
    str,
    # See https://peps.python.org/pep-0484/#covariance-and-contravariance for details on covariance and contravariance
    contravariant=True,
)


# The naming convention is usually adding the suffix -able / -ible.
class Printable(Protocol[T]):
    def print(self, msg: T) -> None:
        ...


class Screen:
    def print(self, msg: str) -> None:
        print(msg)


class TV:
    def print(self, msg: str) -> None:
        print(msg)


class Calculator:
    def print(self, msg: int) -> None:
        print(msg)


class Board:
    # The method `print` is missing.
    ...


def show(item: Printable) -> None:
    import warnings

    try:
        item.print('Meetup Python Rennes')
    except AttributeError as e:
        warnings.warn(str(e))


show(Screen())
show(TV())
show(Calculator())

show(Board())
