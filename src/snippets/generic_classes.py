from typing import (
    List,
    TypeVar,
    Generic,
)

T = TypeVar('T')


class ListOfItems(Generic[T]):
    items: List[T] = []

    def add(self, item: T) -> None:
        self.items.append(item)

    def get_last(self) -> T:
        return self.items.pop()


list_int = ListOfItems[int]()

list_int.add(5)
list_int.add(10)
value = list_int.get_last()


def use_list_of_int(items: ListOfItems[int]) -> None:
    ...


list_float = ListOfItems[float]()

list_float.add('20')
use_list_of_int(list_float)
