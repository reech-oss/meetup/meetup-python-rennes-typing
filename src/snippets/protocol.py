from typing import Protocol


class Screen:
    def print(self, msg: str) -> None:
        print(msg)


class TV:
    def print(self, msg: str) -> None:
        print(msg)


class Calculator:
    #  The method `print` accepts an `int`.
    def print(self, msg: int) -> None:
        print(msg)


class Board:
    # The method `print` is missing.
    ...


# The naming convention is usually adding the suffix -able / -ible.
class Printable(Protocol):
    def print(self, msg: str) -> None:
        ...


def show(item: Printable) -> None:
    import warnings

    try:
        item.print('Meetup Python Rennes')
    except AttributeError as e:
        warnings.warn(str(e))
    except ValueError as e:
        warnings.warn(str(e))


show(Screen())
show(TV())

show(Calculator())
show(Board())
