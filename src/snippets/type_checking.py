from typing import TYPE_CHECKING

if TYPE_CHECKING:
    print('This code will not be called at interpreter runtime')
    # The module will be imported only when my-py is executed.
    from collections.abc import Sequence


# Global type hints must be enclosed in quotes as they are evaluated at interpreter runtime.
def sum_of_square(items: 'Sequence[int]') -> int:
    # Local type hints are not evaluated, no quotes enclosing is required.
    squares: Sequence[int] = [item ** 2 for item in items]
    return sum(squares)


def success() -> None:
    sum_of_square(range(10))


def error() -> None:
    sum_of_square(['1', '2', '3'])
