from typing import (
    List,
    TypeVar,
)

# The generic type can be either `int` or `float`
T = TypeVar('T', int, float)


def power_list(items: List[T]) -> List[T]:
    return [item ** 2 for item in items]


power_list([1, 2, 3])  # All `int`
power_list([1.1, 2.2, 3.3])  # All `float`
power_list([1, 2.2, 3])  # Mix of `int` and `float`

power_list(['1', '2', '3'])
power_list([1, 2.2, '3'])
