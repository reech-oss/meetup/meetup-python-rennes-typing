# Typing In Python

<details>
<summary>Table of contents</summary>

- [Abstract](#abstract)
- [Project Setup](#project-setup)
- [Presentation](#presentation)
- [Tips](#tips)
- [Resources](#resources)

</details>

## Abstract

Project used for the presentation at the [Meetup Python Rennes](https://www.meetup.com/fr-FR/python-rennes/events/298324820/).

## Project Setup

Virtual environment:

```bash
# The location of the Python interpreter may differ on your environment
${HOME}/.pyenv/versions/3.9.16/bin/python3 -m venv .venv
source .venv/bin/activate
python -m ensurepip --upgrade
python -m pip install --upgrade pip
```

Dependencies:

```bash
pip install -r requirements.txt -r requirements-dev.txt
```

## Presentation

The file [Presentation.md](Presentation.md) can be rendered as a standard Markdown, or viewed with the Visual Code extension [vscode-reveal](https://marketplace.visualstudio.com/items?itemName=evilz.vscode-reveal).

## Tips

To clean the project:

```bash
# __pycache__ folders
py3clean src
```

## Resources

- <https://docs.python.org/3/library/typing.html>.
- <https://mypy.readthedocs.io/en/stable/generics.html>.
- [Python 3.12 Preview: Static Typing Improvements](https://realpython.com/python312-typing/).
- [NEW generic / alias syntax for python 3.12 (PEP 695)](https://www.youtube.com/watch?v=YaDYUQ5mD5Q).
- [typing-extensions](https://pypi.org/project/typing-extensions/).
- [mypy Playground](https://mypy-play.net/).
- [Python Type Checking](https://testdriven.io/blog/python-type-checking/).
