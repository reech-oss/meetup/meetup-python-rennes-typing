---
theme : "white"
transition: "default"
slideNumber: True
showNotes: False
width: 120%
margin: 0.1
enableChalkboard: False
progress: True
---

# Typing In Python

Slack Meetup Python Rennes

<center><img src="assets/slack-qr-code-meetup-python-rennes.svg" height="600"></center>

note:
The symbol 💻 means there is a demonstration.

--

## About Me

Michel CARADEC (michel@reech.com)

Lead Software & Data Engineer @ Reech

<center><img src="assets/logo-reech.jpg" height="100"></center>
<center><img src="assets/logo-dekuple.jpg" height="100"></center>

note:
At Reech, we use Python for data gathering.  
Watch the presentation [Python at scale - a Reech case-study](https://www.youtube.com/watch?v=Jj3GRaOMpI8) to know more about Reech.

--

## Agenda

- Recalls On Typing.
- Deep Dive On Typing.
- Generics.
- Protocol.
- Hacks.

*Source code: <https://gitlab.com/reech-oss/meetup/meetup-python-rennes-typing>.*

---

## Demonstration Time

💻

*All the code is in the [Jupyter notebook](./src/meetup-python-rennes-typing.ipynb).*

---

## Wrap Up

Benefits of using type hints and generics:

- Give more **information** on the code.
- Gives more power to **static type checkers** such as [Mypy](https://mypy.readthedocs.io/).
  - Add a **contract** to your code.
  - Check for errors that might have only been seen at **runtime**.
  - Can be integrated to your **CI** with [pre-commit](https://pre-commit.com/).
- Increase **readability**, code **quality**.
- Find bugs and issues before **deployment**.

note:
As a former developer in C++ and C#, both strongly typed languages, I was missing the comfort of typing and generics.

---

## We Want More

- Use [mypy](https://github.com/pre-commit/mirrors-mypy) in pre-commit hooks.
- Check [typeguard](https://pypi.org/project/typeguard/) for run-time type check.
- Check [Pydantic](https://docs.pydantic.dev/) for data validation.

---

## Questions & Answers

Thank you!

😄
